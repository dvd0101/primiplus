#include "randutils.hpp"
#include <iostream>
#include <memory>
#include <primip/all.h>
#include <range/v3/range_concepts.hpp>
#include <vector>

randutils::mt19937_rng rnd;

template <typename>
class Q;

namespace p = primip;

std::unique_ptr<p::mask> random_mask() {
	using namespace primip;

	// return std::make_unique<triangle>(rel_coord{0.1, 0.1}, rel_coord{0.2, 0.2}, rel_coord{0.15, 0.3});
	uint8_t t = rnd.uniform(0, 1);
	if (t == 0) {
		rel_coord a{rnd.uniform(0.f, 1.f), rnd.uniform(0.f, 1.f)};
		rel_coord b{rnd.uniform(0.f, 1.f), rnd.uniform(0.f, 1.f)};
		rel_coord c{rnd.uniform(0.f, 1.f), rnd.uniform(0.f, 1.f)};
		return std::make_unique<triangle>(a, b, c);
	} else {
		rel_coord center{rnd.uniform(0.f, 1.f), rnd.uniform(0.f, 1.f)};
		float width = rnd.uniform(0.f, 0.15f);
		float height = rnd.uniform(0.f, 0.15f);

		return std::make_unique<rectangular>(rel_coord{center.row - height, center.col - width},
		                                     rel_coord{center.row + height, center.col + width});
	}
}

std::unique_ptr<p::mask> choose_random_mask(p::image const& ref, p::image const& target, int attempts) {
	using namespace primip;

	std::unique_ptr<mask> best;
	float error = std::numeric_limits<float>::max();
	while (attempts-- > 0) {
		auto m = random_mask();
		auto const bbox = rect(m->bbox(), ref.size());
		auto const ref_rect = crop(ref, bbox);
		auto const target_rect = crop(target, bbox);
		auto const op = color_mask(*m, ref.pixel(m->center()));


		auto e2 = rms(ref_rect, transform(target_rect, offsetted(op, [bbox = m->bbox()](rel_coord px) {
			                                 rel_coord p = {
			                                     px.row * bbox.height(), px.col * bbox.width(),
			                                 };
			                                 auto x = bbox.top_left + p;
			                                 if (x.row < 0 || x.row > 1 || x.col < 0 || x.col > 1) {
											 std::cout <<"clamp " << x << " <- " << px << "\n";
											 }
			                                 return bbox.top_left + p;
			                             })));

		auto e = rms(ref, transform(target, op));
		if (e < error) {
			error = e;
			best = std::move(m);
		}
	}

	return best;
}

struct draw_cfg {
	uint32_t shapes = 100;
	uint32_t canvas_size = 256;
};

primip::image redraw(primip::image const& src, draw_cfg cfg) {
	using namespace primip;

	auto const in = resize(src, {static_cast<int>(cfg.canvas_size), 0});
	std::cout << "canvas size: " << in.size() << "\n";
	auto tmp = primip::image(in.size());

	std::vector<std::unique_ptr<mask>> masks;
	for (int n = 0; n < cfg.shapes; ++n) {
		// std::cout << n << "\n";
		auto m = choose_random_mask(in, tmp, 20);
		tmp = image(transform(tmp, color_mask(*m, in.pixel(m->center()))));
		masks.push_back(std::move(m));
	}

	auto dst = primip::image(src.size());
	for (auto& mask : masks) {
		primip::draw(dst, *mask, src.pixel(mask->center()));
		// dst = image(transform(dst, color_mask(*mask, src.pixel(mask->center()))));
	}
	return dst;
}

primip::image redraw(primip::image const& src) {
	return redraw(src, {});
}

int main() {
	auto src = primip::load("./img.jpg");
	primip::write(redraw(src), "x.png");
}
