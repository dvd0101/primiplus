#pragma once
#include "core.h"
#include <algorithm>
#include <gsl/span>
#include <stdexcept>
#include <vector>

namespace primip {

	struct io_error : std::runtime_error {
		using std::runtime_error::runtime_error;
	};

	class image {
	  public:
		using pixel_t = std::vector<rgb>;

	  public:
		image(image_size);
		image(image_size, rgb);
		image(image_size, gsl::span<rgb const>);

		template <typename Image>
		image(Image const& src) {
			size_ = src.size();
			pixels_ = src.pixels();
		}

	  public:
		image_size size() const;

		std::vector<rgb>& pixels();
		std::vector<rgb> const& pixels() const;

		rgb& pixel(pixel_coord);
		rgb pixel(pixel_coord) const;

		rgb& pixel(rel_coord);
		rgb pixel(rel_coord) const;

	  private:
		image_size size_;

		std::vector<rgb> pixels_;
	};

	// loads an image from a file
	image load(std::string const&);

	// writes an image to a file (for now only in png format)
	void write(image const&, std::string const&);
}
