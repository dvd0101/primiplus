#pragma once
#include "image.h"
#include "region.h"
#include <functional>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip.hpp>

namespace primip {
	template <typename ImageA, typename ImageB, typename Op>
	class binary {
	  public:
		binary(ImageA const& a, ImageB const& b, Op op) : first_{a}, second_{b}, op_{op} {}

	  public:
		image_size size() const {
			return first.size();
		}

		auto pixels() const {
			return ranges::view::zip_with(op_, first_.get().pixels(), second_.get().pixels());
		}

	  public:
		ImageA const& first() const {
			return first_.get();
		}

		ImageB const& second() const {
			return second_.get();
		}

	  private:
		std::reference_wrapper<ImageA const> first_;
		std::reference_wrapper<ImageB const> second_;
		Op op_;
	};

	template <typename Image, typename Op>
	class transform {
	  public:
		transform(Image const& img, Op op) : img_{img}, op_{op} {}

	  public:
		image_size size() const {
			return img_.get().size();
		}

		auto pixels() const {
			// clang-format off
			return ranges::view::transform(
				img_.get().pixels(),
				[p = pixel_walker(size()), op = op_](rgb const& color) mutable {
					return op.apply(p++, color);
				});
			// clang-format on
		}

	  private:
		std::reference_wrapper<Image const> img_;
		Op op_;
	};

	template <typename Image>
	class crop {
	  public:
		crop(Image const& img, rect r) : img_{img}, r_{r & img.size()} {}

	  public:
		image_size size() const {
			return {r_.width(), r_.height()};
		}

		auto pixels() const {
			return pixel_region(img_.get(), r_);
		}

	  private:
		std::reference_wrapper<Image const> img_;
		rect r_;
	};

}
