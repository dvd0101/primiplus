#pragma once
#include "core.h"
#include <iostream>

namespace primip {
	class mask {
	  public:
		virtual ~mask() = default;

		virtual rel_rect bbox() const = 0;
		virtual rel_coord center() const = 0;
		virtual bool test(rel_coord) const = 0;
	};

	class rectangular : public mask {
	  public:
		rectangular(rel_coord top_left, rel_coord bottom_right);

		rel_rect bbox() const override;
		rel_coord center() const override;
		bool test(rel_coord) const override;

	  private:
		rel_rect r_;
	};

	class triangle : public mask {
	  public:
		triangle(rel_coord, rel_coord, rel_coord);

		rel_rect bbox() const override;
		rel_coord center() const override;
		bool test(rel_coord) const override;

	  private:
		rel_coord a_, b_, c_;
		rel_coord center_;
		rel_rect r_;
	};

	class color_mask {
	  public:
		color_mask(mask const& m, rgb color) : mask_(&m), color_(color) {}

		rgb apply(rel_coord pixel, rgb original) const {
			return mask_->test(pixel) ? (color_ * 0.6 + original * 0.4) : original;
		}

	  private:
		mask const* mask_;
		rgb color_;
	};

	template <typename Op, typename Func>
	class offsetted {
	  public:
		offsetted(Op const& op, Func f) : op_(&op), f_(f) {}

		rgb apply(rel_coord pixel, rgb original) const {
			return op_->apply(f_(pixel), original);
		}

	  private:
		Op const* op_;
		Func f_;
	};
}
