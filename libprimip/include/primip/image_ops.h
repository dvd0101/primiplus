#pragma once
#include "composed_image.h"
#include "image.h"
#include <cmath>

namespace primip {
	struct op_error : std::runtime_error {
		using std::runtime_error::runtime_error;
	};

	template <typename ImageA, typename ImageB>
	auto difference(ImageA const& im1, ImageB const& im2) {
		if (im1.size() != im2.size()) {
			throw op_error("the two images must have the same size");
		}

		return binary(im1, im2, std::minus<>{});
	}

	template <typename ImageA, typename ImageB>
	float rms(ImageA const& im1, ImageB const& im2) {
		float r = 0;
		float g = 0;
		float b = 0;
		int n = 0;
		for (auto const& px : difference(im1, im2).pixels()) {
			r += px.r * px.r;
			g += px.g * px.g;
			b += px.b * px.b;
			++n;
		}
		return std::sqrt((r + g + b) / n);
	}

	class mask;

	void draw(image&, mask const&, rgb);

	namespace details {
		/**
		 * Interpolate the point x between p1 and p2 using a the catmull-rom cubic
		 * interpolation.
		 *
		 * x is the continuos value between p1 (x=0) and p2 (x=1).
		 *
		 * p0 and p3 are the previous and the next values, and are used to
		 * interpolate the derivative functions.
		 */
		inline float catmull(float p0, float p1, float p2, float p3, float x) {
			// see: http://entropymine.com/imageworsener/bicubic/
			// see: http://www.paulinternet.nl/?page=bicubic
			const float a = -0.5 * p0 + 3.0 / 2 * p1 - 3.0 / 2 * p2 + 0.5 * p3;
			const float b = p0 - 5.0 / 2 * p1 + 2.0 * p2 - 0.5 * p3;
			const float c = -0.5 * p0 + 0.5 * p2;
			const float d = p1;
			return a * x * x * x + b * x * x + c * x + d;
		}

		inline rgb interpolate(std::array<rgb, 16> const& pixels, float u, float v) {
			float rr[4], rg[4], rb[4];
			for (int c = 0; c < 4; ++c) {
				int const ix = c * 4;
				rr[c] = catmull(pixels[ix + 0].r, pixels[ix + 1].r, pixels[ix + 2].r, pixels[ix + 3].r, u);
				rg[c] = catmull(pixels[ix + 0].g, pixels[ix + 1].g, pixels[ix + 2].g, pixels[ix + 3].g, u);
				rb[c] = catmull(pixels[ix + 0].b, pixels[ix + 1].b, pixels[ix + 2].b, pixels[ix + 3].b, u);
			}
			int r = catmull(rr[0], rr[1], rr[2], rr[3], v);
			int g = catmull(rg[0], rg[1], rg[2], rg[3], v);
			int b = catmull(rb[0], rb[1], rb[2], rb[3], v);
			return {
			    static_cast<uint8_t>(std::clamp(r, 0, 255)),
			    static_cast<uint8_t>(std::clamp(g, 0, 255)),
			    static_cast<uint8_t>(std::clamp(b, 0, 255)),
			};
		}
	}

	template <typename Image>
	image resize(Image const& src, image_size size) {
		if (size.width == 0 || size.height == 0) {
			float r = static_cast<float>(src.size().width) / src.size().height;
			if (size.width == 0) {
				size.width = std::round(size.height * r);
			} else {
				size.height = std::round(size.width / r);
			}
		}
		image dst(size);

		float const hR = static_cast<float>(size.height) / src.size().height;
		float const wR = static_cast<float>(size.width) / src.size().width;
		for (int y = 0; y < size.height; ++y) {
			float const v = y / hR;
			int const sy = static_cast<int>(v);
			for (int x = 0; x < size.width; ++x) {
				float const u = x / wR;
				int const sx = static_cast<int>(u);

				using px = pixel_coord;
				std::array<rgb, 16> const quad = {
				    src.pixel(px{sy - 1, sx - 1}),
				    src.pixel(px{sy - 1, sx + 0}),
				    src.pixel(px{sy - 1, sx + 1}),
				    src.pixel(px{sy - 1, sx + 2}),

				    src.pixel(px{sy + 0, sx - 1}),
				    src.pixel(px{sy + 0, sx + 0}),
				    src.pixel(px{sy + 0, sx + 1}),
				    src.pixel(px{sy + 0, sx + 2}),

				    src.pixel(px{sy + 1, sx - 1}),
				    src.pixel(px{sy + 1, sx + 0}),
				    src.pixel(px{sy + 1, sx + 1}),
				    src.pixel(px{sy + 1, sx + 2}),

				    src.pixel(px{sy + 2, sx - 1}),
				    src.pixel(px{sy + 2, sx + 0}),
				    src.pixel(px{sy + 2, sx + 1}),
				    src.pixel(px{sy + 2, sx + 2}),
				};
				dst.pixel(pixel_coord{y, x}) = details::interpolate(quad, u - sx, v - sy);
			}
		}

		return dst;
	}
}
