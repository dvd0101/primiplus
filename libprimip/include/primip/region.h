#pragma once
#include "image.h"
#include <range/v3/core.hpp>
#include <type_traits>

namespace primip {
	template <typename Image>
	class pixel_region : public ranges::view_facade<pixel_region<Image>> {
		static_assert(std::is_same_v<std::remove_cv_t<Image>, image>,
		              "pixel_region can only be used with an image (possibly const) reference");

		using iterator_t = std::conditional_t< //
		    std::is_const_v<Image>,            //
		    std::vector<rgb>::const_iterator,  //
		    std::vector<rgb>::iterator>;       //

		using rgb_t = std::conditional_t< //
		    std::is_const_v<Image>,       //
		    rgb const,                    //
		    rgb>;                         //

	  public:
		pixel_region() : rows_{0} {}

		pixel_region(Image& img, pixel_coord tl, pixel_coord br) {
			tl.row = std::max(tl.row, 0);
			tl.col = std::max(tl.col, 0);
			br.row = std::min(br.row, img.size().height - 1);
			br.col = std::min(br.col, img.size().width - 1);

			it_ = img.pixels().begin() + img.size().width * tl.row + tl.col;
			counter_ = 0;
			width_ = br.col - tl.col + 1;
			rows_ = br.row - tl.row + 1;
			stride_ = img.size().width - br.col - 1 + tl.col;
		}

		pixel_region(Image& img, rect r) : pixel_region(img, r.top_left, r.bottom_right) {}

	  private:
		friend ranges::range_access;

		rgb_t& read() const {
			return *it_;
		}

		void next() {
			++it_;
			if (++counter_ == width_) {
				counter_ = 0;
				--rows_;
				if (rows_) {
					it_ += stride_;
				}
			}
		}

		bool equal(ranges::default_sentinel) const {
			return rows_ == 0;
		}

		bool equal(pixel_region const& other) const {
			if (rows_ == 0) {
				return other.rows_ == 0;
			} else {
				return it_ == other.it_;
			}
		}

	  private:
		iterator_t it_;
		int counter_;
		int width_;
		int rows_;
		int stride_;
	};

	class coord_region : public ranges::view_facade<coord_region> {
	  public:
		coord_region() = default;

		coord_region(rect r) : pw{r} {}

		coord_region(pixel_coord tl, pixel_coord br) : coord_region{rect{tl, br}} {}

	  private:
		friend ranges::range_access;

		pixel_coord const& read() const {
			return pw;
		}

		void next() {
			++pw;
		}

		bool equal(ranges::default_sentinel) const {
			return !pw;
		}

		bool equal(coord_region const& other) const {
			if (!pw) {
				return !other.pw;
			}
			return static_cast<pixel_coord>(pw) == static_cast<pixel_coord>(other.pw);
		}

	  private:
	  	pixel_walker pw;
	};
}
