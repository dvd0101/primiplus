#pragma once
#include <cstdint>
#include <iosfwd>

namespace primip {
	struct rgb {
		uint8_t r;
		uint8_t g;
		uint8_t b;
	};

	rgb operator-(rgb, rgb);
	rgb operator+(rgb, rgb);
	rgb operator*(rgb, float);
	std::ostream& operator<<(std::ostream&, rgb);

	struct image_size {
		int width;
		int height;

		bool operator==(image_size) const;
		bool operator!=(image_size) const;
	};

	std::ostream& operator<<(std::ostream&, image_size);

	namespace details {
		template <typename Derived, typename T>
		struct coord {
			T row;
			T col;

			bool operator==(Derived b) const {
				return row == b.row && col == b.col;
			}

			bool operator!=(Derived b) const {
				return !(*this == b);
			}

			bool operator>(Derived b) const {
				return row > b.row && col > b.col;
			}

			bool operator>=(Derived b) const {
				return row >= b.row && col >= b.col;
			}

			bool operator<(Derived b) const {
				return row < b.row && col < b.col;
			}

			bool operator<=(Derived b) const {
				return row <= b.row && col <= b.col;
			}

			Derived operator+(Derived b) const {
				return {
				    row + b.row, col + b.col,
				};
			}

			Derived operator-(Derived b) const {
				return {
				    row - b.row, col - b.col,
				};
			}

			Derived operator*(T k) const {
				return {
				    row * k, col * k
				};
			}
		};
	}

	struct rel_coord : details::coord<rel_coord, float> {};
	struct pixel_coord : details::coord<pixel_coord, int> {
		pixel_coord() = default;
		pixel_coord(int, int);
		pixel_coord(rel_coord, image_size);
	};

	std::ostream& operator<<(std::ostream&, rel_coord);
	std::ostream& operator<<(std::ostream&, pixel_coord);

	struct rel_rect {
		rel_coord top_left;
		rel_coord bottom_right;

		float width() const;
		float height() const;
	};
	std::ostream& operator<<(std::ostream&, rel_rect const&);

	struct rect {
		rect() = default;
		rect(pixel_coord, pixel_coord);
		rect(rel_rect, image_size);
		rect(image_size);

		int width() const;
		int height() const;

		pixel_coord top_left;
		pixel_coord bottom_right;
	};

	bool intersect(rect const&, rect const&);
	rect operator&(rect const&, rect const&);
	std::ostream& operator<<(std::ostream&, rect const&);

	class pixel_walker {
	  public:
		// construct an already complete pixel_walker
		pixel_walker();
		pixel_walker(image_size);
		pixel_walker(rect);

		pixel_walker& operator++();
		pixel_walker operator++(int);

		operator bool() const;

		operator pixel_coord const&() const;

		operator rel_coord() const;

	  private:
		rect r_;
		pixel_coord pos_;
	};
}
