#include <cmath>
#include <iostream>
#include <primip/masks.h>

namespace primip {
	rectangular::rectangular(rel_coord top_left, rel_coord bottom_right) : r_{top_left, bottom_right} {}

	rel_rect rectangular::bbox() const {
		return r_;
	}

	rel_coord rectangular::center() const {
		return {
		    r_.top_left.row + (r_.bottom_right.row - r_.top_left.row) / 2,
		    r_.top_left.col + (r_.bottom_right.col - r_.top_left.col) / 2,
		};
	}

	bool rectangular::test(rel_coord px) const {
		return px >= r_.top_left && px <= r_.bottom_right;
	}

	triangle::triangle(rel_coord a, rel_coord b, rel_coord c)
	    : a_{a},
	      b_{b},
	      c_{c}
	{
		float const la = std::sqrt((b_.col - c_.col) * (b_.col - c_.col) + (b_.row - c_.row) * (b_.row - c_.row));
		float const lb = std::sqrt((a_.col - c_.col) * (a_.col - c_.col) + (a_.row - c_.row) * (a_.row - c_.row));
		float const lc = std::sqrt((b_.col - a_.col) * (b_.col - a_.col) + (b_.row - a_.row) * (b_.row - a_.row));

		center_.col = (la * a_.col + lb * b_.col + lc * c_.col) / (la + lb + lc);
		center_.row = (la * a_.row + lb * b_.row + lc * c_.row) / (la + lb + lc);

		r_.top_left.row = std::min(std::min(a_.row, b_.row), c_.row);
		r_.top_left.col = std::min(std::min(a_.col, b_.col), c_.col);
		r_.bottom_right.row = std::max(std::max(a_.row, b_.row), c_.row);
		r_.bottom_right.col = std::max(std::max(a_.col, b_.col), c_.col);
	}

	rel_rect triangle::bbox() const {
		return r_;
	}

	rel_coord triangle::center() const {
		return center_;
	}

	bool triangle::test(rel_coord px) const {
		float const d = ((b_.row - c_.row) * (a_.col - c_.col) + (c_.col - b_.col) * (a_.row - c_.row));
		float const s = ((b_.row - c_.row) * (px.col - c_.col) + (c_.col - b_.col) * (px.row - c_.row)) / d;
		float const t = ((c_.row - a_.row) * (px.col - c_.col) + (a_.col - c_.col) * (px.row - c_.row)) / d;
		float const u = 1 - s - t;
		return s >= 0 && s <= 1 && t >= 0 && t <= 1 && u >= 0 && u <= 1;
	}
}
