#include <algorithm>
#include <cmath>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <primip/core.h>
#include <iostream>

namespace primip {
	using namespace fmt::literals;

	rgb operator-(rgb c1, rgb c2) {
		return {
		    static_cast<uint8_t>(std::abs(static_cast<int>(c1.r) - static_cast<int>(c2.r))),
		    static_cast<uint8_t>(std::abs(static_cast<int>(c1.g) - static_cast<int>(c2.g))),
		    static_cast<uint8_t>(std::abs(static_cast<int>(c1.b) - static_cast<int>(c2.b))),
		};
	}

	rgb operator+(rgb c1, rgb c2) {
		return {
		    static_cast<uint8_t>(std::abs(static_cast<int>(c1.r) + static_cast<int>(c2.r))),
		    static_cast<uint8_t>(std::abs(static_cast<int>(c1.g) + static_cast<int>(c2.g))),
		    static_cast<uint8_t>(std::abs(static_cast<int>(c1.b) + static_cast<int>(c2.b))),
		};
	}

	rgb operator*(rgb c, float a) {
		return {
		    static_cast<uint8_t>(std::abs(c.r * a)),
		    static_cast<uint8_t>(std::abs(c.g * a)),
		    static_cast<uint8_t>(std::abs(c.b * a)),
		};
	}

	std::ostream& operator<<(std::ostream& os, rgb c) {
		return os << "({},{},{})"_format(c.r, c.g, c.b);
	}

	std::ostream& operator<<(std::ostream& os, image_size s) {
		return os << "[{},{}]"_format(s.width, s.height);
	}

	pixel_coord::pixel_coord(int r, int c) : details::coord<pixel_coord, int>{r, c} {}

	pixel_coord::pixel_coord(rel_coord r, image_size s)
	    : pixel_coord{static_cast<int>(r.row * s.height), static_cast<int>(r.col * s.width)} {}

	std::ostream& operator<<(std::ostream& os, pixel_coord px) {
		return os << "px({},{})"_format(px.row, px.col);
	}

	std::ostream& operator<<(std::ostream& os, rel_coord px) {
		return os << "px({},{})"_format(px.row, px.col);
	}

	float rel_rect::width() const {
		return bottom_right.col - top_left.col;
	}

	float rel_rect::height() const {
		return bottom_right.row - top_left.row;
	}

	rect::rect(pixel_coord tl, pixel_coord br) : top_left{tl}, bottom_right{br} {
		assert(tl.col <= br.col);
		assert(tl.row <= br.row);
	}

	rect::rect(rel_rect r, image_size s) : top_left{r.top_left, s}, bottom_right{r.bottom_right, s} {}

	rect::rect(image_size s) : top_left{0, 0}, bottom_right{s.height, s.width} {}

	int rect::width() const {
		return bottom_right.col - top_left.col + 1;
	}

	int rect::height() const {
		return bottom_right.row - top_left.row + 1;
	}

	bool intersect(rect const& r1, rect const& r2) {
		return !(
			r2.top_left.col > r1.bottom_right.col
			|| r2.bottom_right.col < r1.top_left.col
			|| r2.top_left.row > r1.bottom_right.row
			|| r2.bottom_right.row < r1.top_left.row
        );
	}

	rect operator&(rect const& r1, rect const& r2) {
		if (!intersect(r1, r2)) {
			return {pixel_coord{0, 0}, pixel_coord{0, 0}};
		}
		return {
			std::max(r1.top_left, r2.top_left),
			std::min(r1.bottom_right, r2.bottom_right)
		};
	}

	std::ostream& operator<<(std::ostream& os, rect const& r) {
		return os << "({},{})"_format(r.top_left, r.bottom_right);
	}

	std::ostream& operator<<(std::ostream& os, rel_rect const& r) {
		return os << "({},{})"_format(r.top_left, r.bottom_right);
	}

	bool image_size::operator==(image_size s) const {
		return width == s.width && height == s.height;
	}

	bool image_size::operator!=(image_size s) const {
		return !(*this == s);
	}

	pixel_walker::pixel_walker() : r_{pixel_coord(0, 0), pixel_coord(0, 0)}, pos_{1, 1} {}

	pixel_walker::pixel_walker(rect r) : r_{r}, pos_{r.top_left} {}

	pixel_walker::pixel_walker(image_size s)
	    : pixel_walker{rect(pixel_coord{0, 0}, pixel_coord{s.height - 1, s.width - 1})} {}

	pixel_walker& pixel_walker::operator++() {
		++pos_.col;
		if (pos_.col > r_.bottom_right.col) {
			pos_.col = r_.top_left.col;
			++pos_.row;
		}
		return *this;
	}

	pixel_walker pixel_walker::operator++(int) {
		auto out = *this;
		++(*this);
		return out;
	}

	pixel_walker::operator bool() const {
		return pos_.col <= r_.bottom_right.col && pos_.row <= r_.bottom_right.row;
	}

	pixel_walker::operator pixel_coord const&() const {
		assert(*this);
		return pos_;
	}

	pixel_walker::operator rel_coord() const {
		assert(*this);
			return {
				static_cast<float>(pos_.row) / r_.height(),
				static_cast<float>(pos_.col) / r_.width(),
			};
		}
}
