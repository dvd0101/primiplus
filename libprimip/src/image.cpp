#include "stb_image.h"
#include "stb_image_write.h"
#include <algorithm>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <memory>
#include <primip/image.h>

namespace {
	using stb_ptr = std::unique_ptr<uint8_t, decltype(&stbi_image_free)>;

	auto make_stb_ptr(uint8_t* ptr) {
		return stb_ptr(ptr, &::stbi_image_free);
	}
}

namespace primip {
	using namespace fmt::literals;

	image load(std::string const& file_name) {
		int w, h, n;
		auto file_data = make_stb_ptr(stbi_load(file_name.c_str(), &w, &h, &n, 3));
		if (!file_data) {
			throw io_error("error reading: {}"_format(file_name));
		}
		auto ptr = reinterpret_cast<rgb*>(file_data.get());
		return image({w, h}, {ptr, w * h});
	}

	void write(image const& img, std::string const& file_name) {
		int err = stbi_write_png(file_name.c_str(), img.size().width, img.size().height, 3, img.pixels().data(), 0);
		if (err == 0) {
			throw io_error("error writing to: {}"_format(file_name));
		}
	}

	image::image(image_size s) : size_(s), pixels_(s.width * s.height, {0, 0, 0}) {}
	image::image(image_size s, rgb c) : size_(s), pixels_(s.width * s.height, c) {}

	image::image(image_size s, gsl::span<rgb const> data) : size_(s) {
		int n = s.width * s.height;
		if (data.size() != n) {
			throw io_error("size mismatch");
		}
		pixels_.reserve(n);
		std::copy(data.begin(), data.end(), std::back_inserter(pixels_));
	}

	image_size image::size() const {
		return size_;
	}

	rgb image::pixel(pixel_coord px) const {
		int const r = std::clamp(px.row, 0, size_.height - 1);
		int const c = std::clamp(px.col, 0, size_.width - 1);
		return pixels_[r * size_.width + c];
	}

	rgb& image::pixel(pixel_coord px) {
		int const r = std::clamp(px.row, 0, size_.height - 1);
		int const c = std::clamp(px.col, 0, size_.width - 1);
		return pixels_[r * size_.width + c];
	}

	rgb image::pixel(rel_coord px) const {
		return pixel(pixel_coord{
		    static_cast<int>(px.row * size_.height),
		    static_cast<int>(px.col * size_.width),
		});
	}

	rgb& image::pixel(rel_coord px) {
		return pixel(pixel_coord{
		    static_cast<int>(px.row * size_.height),
		    static_cast<int>(px.col * size_.width),
		});
	}

	std::vector<rgb>& image::pixels() {
		return pixels_;
	}

	std::vector<rgb> const& image::pixels() const {
		return pixels_;
	}
}
