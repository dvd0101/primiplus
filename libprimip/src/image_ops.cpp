#include <primip/image_ops.h>
#include <primip/masks.h>
#include <primip/region.h>
#include <range/v3/algorithm/for_each.hpp>

namespace primip {
	void draw(image& img, mask const& m, rgb c) {
		rect bbox(m.bbox(), img.size());
		ranges::for_each(coord_region(bbox), [&](pixel_coord px) {
			rel_coord coord{static_cast<float>(px.row) / img.size().height,
			                static_cast<float>(px.col) / img.size().width};
			if (m.test(coord))
				img.pixel(px) = c;
		});
	}
}
